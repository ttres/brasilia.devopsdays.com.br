+++
categories = ["Development", "Operation"]
date = "2016-08-24T10:42:39-03:00"
draft = true
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "Chamadas de Trabalhos"
description = "Compartilhe seu conhecimento"

+++
<!--# Chamadas de Trabalhos

***<p class="text-center bg-success text-info">Compartilhe seu conhecimento</p>*** -->

Venha ao ***DevOpsDays Brasília*** e apresente palestras para centenas de entusiastas DevOps.

A chamada está aberta e ao enviar sua proposta você pode ser um de nossos palestrantes nas várias modalidades de sessões que teremos.

---

**Atenção:**
<p class="text-center bg-warning text-danger">A submissão de propostas será finalizada em 30 de setembro, 2016.</p>

---

Como palestrante você:

- terá entrada franca à converência;
- aproveitará sua estadia em Brasília;
- será (re)conhecido como referência no tema de sua palestra e como formador de opnião e tendências;

Teremos três formas de sessões:

1. **Proposta para palestra/painel** : até 30 minutos no autiório principal.
1. **Palestra Relâmpago**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão até 20 sessões com até 5 minutos cada e com slides trocando automaticamente. Apresentados no auditório principal e para todos os presentes.
1. **Sessões de Espaço Aberto**: Você é bem vindo para discutir e interagir propondo uma sessão ao vivo nos espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) - mesmo que você não tenha previamente uma apresentação pronta.


### **Regras**

Nosso critério para escolha de propostas são:

- **conteúdo original**: Conteúdo ainda não apresentado em outras conferências, ou um novo ângulo de um problema existente
- **novos palestrantes**: Pessoas novas e que tem coisas interessante a dizer; Nós precisamos ouvir todos
- **sem vendas**: Nós valorizamos fornecedores e patrocinadores, porém nós apenas não achamos que é o local adequado para isso. Você pode demonstrar no espaço reservado a fornecedores e patrocinadores.

Requisitos quanto ao conteúdo:

- Seja específico… descrição com ao menos 20 linhas é algo desejável
- Detalhes são bons… explique porque sua palestra é interessante
- Proponha sua própria palestra
- Indicações são bem vindas… Se você conhece alguém com conteúdo/relevância em DevOps incentive-o a enviar proposta de palestra, ainda assim pode nos indicar através do email <span class="btn btn-link">[organizers-brasilia-2016@devopsdays.org](<mailto: organizers-brasilia-2016@devopsdays.org>)</span>
- Múltiplas propostas são bem vindas… apenas sigas todas as regras

---

### Formulário de propostas

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc2dKOvCB1fVKnRkUD2C0dJ_f0QzZBIDQTeGMd9grSSYEibcg/viewform?embedded=true" width="760" height="1200" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
